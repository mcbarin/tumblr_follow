var webdriver = require('selenium-webdriver');
//changesdsdsds
var driver = new webdriver.Builder().
   withCapabilities(webdriver.Capabilities.firefox()).
   build();

// For maximizing the window
driver.manage().window().maximize();

var username1= "tumblr_movielalagifs@movielala.com";
var username2 = "emojitrailers@gmail.com";
var password1 = "xxx";
var password2 = "xxxx";
var keywordsForSearch1 = ["gifs","gif","gifmaker","funny gif","animated gif","cool gifs"];
var keywordsForSearch2 = ["movies","movie trailers","teasers","funny movies","hollywood"];

var followCount = 0;
var loveCount = 0;

var myUsername = "asdasd@gmail.com";
var myPassword = "asdadasd";
// With given username and password, it logins into tumblr.
tumblrLogin(myUsername,myPassword);

// It searches for keywords one by one. Then follows and likes the posts on the page.
for(var i=0;i<keywordsForSearch2.length;i++){
  search(keywordsForSearch2[i]);
  followAndLikeOnPage();
}


function tumblrLogin(username,password){
  driver.get('https://www.tumblr.com/login');
  driver.findElement(webdriver.By.id('signup_email')).sendKeys(username);
  driver.findElement(webdriver.By.id('signup_password')).sendKeys(password);
  driver.findElement(webdriver.By.id('signup_forms_submit')).click();
}
// In every page, It will follow and like 20 of them.
function followAndLikeOnPage(){
  for(var i=1;i<20;i++){

    // For follow
   if(isFollowed(i)){
      var xpathForPost = xpathCreatorForFollow(i);
      var follow = driver.findElement(webdriver.By.xpath(xpathForPost));
      follow.click();
      followCount++;
    }

    //driver.manage().timeouts().implicitlyWait(1000);

    driver.sleep(1000); // Wait 1 second.

    // For like
    if(isLiked(i)){
      var xpathForLike = xpathCreatorForLike(i);
      var like = driver.findElement(webdriver.By.xpath(xpathForLike));
      like.click();
      loveCount++;
    }

    //driver.manage().window().scrollBy(0,200);
    //driver.manage().timeouts().implicitlyWait(1000);

    driver.sleep(1000); // Wait 1 second.
  }
}



function search(keyword){
  var searchKeyword = "https://www.tumblr.com/search/";
  searchKeyword += keyword;
  searchKeyword = searchKeyword.replace(" ","+");
  console.log("Now navigating to " + searchKeyword);
  driver.get(searchKeyword);
}

// gives the follow button xpath of the article (=post from tumblr)
function xpathCreatorForFollow(postNumber){
  var xpathValue = "//article[";
  xpathValue += postNumber.toString();
  xpathValue += "]/header/div/div/div/a/div[1][@class='follow-text']";
  return xpathValue;
}

// gives the like button xpath of the article (=post from tumblr)
function xpathCreatorForLike(postNumber){
  var xpathValue = "//article[";
  xpathValue += postNumber.toString();
  xpathValue += "]/div/div[2]/div/div[@class='post_control like']";

  return xpathValue;
}

function isFollowed(postNumber){
  var xpathForPost = "//article[";
  xpathForPost += postNumber.toString();
  xpathForPost += "]/header/div/div/div/a[@class='follow_link worded-follow-button show-unfollow']";
  Boolean result = true;
  result = (driver.isElementPresent(webdriver.By.xpath(xpathForPost)));
  return result;
}

function isLiked(postNumber){
  var xpathForPost = "//article[";
  xpathForPost += postNumber.toString();
  xpathForPost += "]/div/div[2]/div/div[@class='post_control like']";
  Boolean result = true;
  result =  (driver.isElementPresent(webdriver.By.xpath(xpathForPost)));

  return result;
}



// ###############################################

// //article[1]/div/div[2]/div/div[@class='post_control like']    //When it is not liked
// //article[1]/div/div[2]/div/div[@class='post_control like liked']    // When it is liked

//  //article[1]/header/div/div/div/a[@class='follow_link worded-follow-button show-unfollow']  // When it is not followed
//  //article[1]/header/div/div/div/a[@class='follow_link worded-follow-button show-unfollow is-following']

// ###############################################

//console.log(followCount);
//console.log(loveCount);

/*
var followButton = driver.findElement(webdriver.By.xpath("//article[3]/header/div/div/div/a/div[1][@class='follow-text']"));
 followButton.click();
*/

/*   // Icon view works but unnecessary for now .
driver.wait(function(){
  var listView = driver.findElement(webdriver.By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/a[2]/i[@class='icon_view_list']"));
  listView.click();
},4000);
*/

driver.quit();
